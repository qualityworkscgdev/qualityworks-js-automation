module.exports = {

  'Step 1: Login as Timely User': function(browser){
      browser
        .url('http://timely-qw.herokuapp.com/')
        .waitForElementVisible('body', 10000, 'Verfiy that body is visible')
        .waitForElementVisible('#loginBtn', 10000,'Verify that login button is visible')
        .click('#loginBtn')
        .waitForElementVisible('#email',10000,'Verify that email field is visible')
        .setValue('#email','timelytester@gmail.com')
        .setValue('#password', 'Password123')
        .click("button[type='submit']")
        .assert.containsText('div.header', 'Timely Tester')
    },

    'Step 2: Log Out': function(browser){
      browser
        .click('#btnLogout')
        .waitForElementVisible("div[class='ui green ok inverted button']",10000,'Verify that Confirm Log out is visible')
        .click("div[class='ui green ok inverted button']")
        .waitForElementVisible('#loginBtn',10000, 'Verify that login button is visible')
        .assert.visible('#loginBtn')
        .end();
    }
};