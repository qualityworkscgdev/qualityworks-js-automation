# QualityWorks Nightwatch Demo

This is a simple demo of the Nighwatch.js framework for browser automated testing.

### Getting Started

Install [Node.js](https://nodejs.org/en/) and then clone this repository:

```sh 
$ git clone https://wharding_qualityworkscg@bitbucket.org/qualityworkscgdev/qualityworks-js-automation.git
$ cd qualityworks-js-automation
$ npm install
$ npm test
```

** Note: ** Ensure ** Firefox v45.0.2 ** or ** higher ** is installed.

### Useful links

####[Nightwatch.js](http://nightwatchjs.org/)
Write End-to-End tests in Node.js quickly and effortlessly that run against a Selenium server.

####[nodeqa](http://nodeqa.io/) 
Compare full stack testing technologies for node apps